from flask_restplus import Api
from flask import Blueprint

from .main.auth.routes import AuthDto
from .main.user.routes import UserDto
from .main.board.routes import BoardDto, PostDto, CommentDto, ImageDto

blueprint = Blueprint('api', __name__)

api = Api(
    blueprint,
    title='CROSS BOARD BACKEND API',
    version='1.0',
    description='a boilerplate for flask restplus web service',
)

api.add_namespace(AuthDto.api, path='/auth')
api.add_namespace(UserDto.api, path='/users')
api.add_namespace(BoardDto.api, path='/boards')
api.add_namespace(PostDto.api, path='/posts')
api.add_namespace(CommentDto.api, path='/comments')
api.add_namespace(ImageDto.api, path='/images')
