from flask_restplus import Namespace, fields
from marshmallow import Schema, EXCLUDE
from app.main.util.validator import AuthValidator


# Swagger Document 용
class AuthDto:
    api = Namespace(name='auth', description='인증 관련 작업')
    login_model = api.model(
        name='login_model',
        model={
            'phone_code': fields.String(required=True, description='Phone code'),
            'phone_number': fields.String(required=True, description='Phone number'),
            'password': fields.String(required=True, description='Password'),
        }
    )
    password_reset_model = api.model(
        name='password_reset_model',
        model={
            'phone_code': fields.String(required=True, description='Phone code'),
            'phone_number': fields.String(required=True, description='Phone number'),
            'password': fields.String(required=True, description='Password'),
            'verification_code': fields.String(required=True, description='Verification code'),
        }
    )
    password_change_model = api.model(
        name='password_change_model',
        model={
            'password': fields.String(required=True, description='Password'),
            'new_password': fields.String(required=True, description='New password'),
        }
    )
    phone_change_model = api.model(
        name='phone_change_model',
        model={
            'phone_code': fields.String(required=True, description='Phone code'),
            'phone_number': fields.String(required=True, description='Phone number'),
            'verification_code': fields.String(required=True, description='Verification code'),
        }
    )
    mobile_verification_model = api.model(
        name='mobile_verification_model',
        model={
            'phone_code': fields.String(required=True, description='Phone code'),
            'phone_number': fields.String(required=True, description='Phone number'),
            'purpose': fields.String(required=True, description='Purpose'),
            'verification_code': fields.String(required=True, description='Verification code'),
        }
    )


# 실제 데이터 검증 용
class LoginSchema:
    class Post(Schema):
        phone_code = AuthValidator.phone_code
        phone_number = AuthValidator.phone_number
        password = AuthValidator.password


class PasswordResetSchema:
    class Post(Schema):
        phone_code = AuthValidator.phone_code
        phone_number = AuthValidator.phone_number
        password = AuthValidator.password
        verification_code = AuthValidator.mobile_verification_code


class PasswordChangeSchema:
    class Post(Schema):
        password = AuthValidator.password
        new_password = AuthValidator.password
        verification_code = AuthValidator.mobile_verification_code

    class Put(Schema):
        password = AuthValidator.password
        new_password = AuthValidator.password

        class Meta:
            unknown = EXCLUDE


class PhoneChangeSchema:
    class Post(Schema):
        phone_code = AuthValidator.phone_code
        phone_number = AuthValidator.phone_number
        purpose = AuthValidator.mobile_verification_purpose
        verification_code = AuthValidator.mobile_verification_code


class MobileVerificationSchema:
    class Get(Schema):
        phone_code = AuthValidator.phone_code
        phone_number = AuthValidator.phone_number
        purpose = AuthValidator.mobile_verification_purpose

    class Post(Schema):
        phone_code = AuthValidator.phone_code
        phone_number = AuthValidator.phone_number
        purpose = AuthValidator.mobile_verification_purpose
        verification_code = AuthValidator.mobile_verification_code
