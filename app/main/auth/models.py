import datetime
import jwt
from . import db, key
from flask_bcrypt import Bcrypt
from enum import Enum

from ..board.models import PostStatus, CommentStatus


class AccountStatus(Enum):
    # 1:active, 0:탈퇴신청, -1:탈퇴완료
    NORMAL = 1
    SECEDE_APPLIED = 0
    SECEDED_COMPLETED = -1


class SecedeStatus(Enum):
    # 1:active, 0:탈퇴신청, -1:탈퇴완료
    APPROVED = 1
    APPLIED = 0
    REJECTED = -1


class User(db.Model):
    """
    User Model for storing user related details
    """
    __tablename__ = "User"

    # Account
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(db.String(100), unique=True)
    phone_code = db.Column(db.String(5), nullable=False)
    phone_number = db.Column(db.String(20), nullable=False)
    password_hash = db.Column(db.String(100), nullable=False)
    account_status = db.Column(db.SmallInteger, nullable=False, default=AccountStatus.NORMAL.value)
    is_superuser = db.Column(db.Boolean, nullable=False, default=False)

    # Profile
    nickname = db.Column(db.String(50), nullable=False)
    lang = db.Column(db.String(2), nullable=False, default='en')
    preferred_country = db.Column(db.String(2))

    # Registered Date / Last Login
    registered_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    last_login_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

    # Agreement
    agree_terms = db.Column(db.Boolean, nullable=False, default=False)
    agree_privacy = db.Column(db.Boolean, nullable=False, default=False)
    agree_sms_notice = db.Column(db.Boolean, nullable=False, default=False)
    agree_sms_marketing = db.Column(db.Boolean, nullable=False, default=False)

    __table_args__ = (db.UniqueConstraint('phone_code', 'phone_number'),)

    def __repr__(self):
        return "<User {},{} '{}'>".format(self.phone_code, self.phone_number, self.nickname)

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @property
    def count_posts(self):
        from app.main.board.models import Post
        return Post.query.filter_by(user_id=self.id, post_status=PostStatus.NORMAL.value).count()

    @property
    def count_comments(self):
        from app.main.board.models import Comment
        return Comment.query.filter_by(user_id=self.id, comment_status=CommentStatus.NORMAL.value).count()

    @password.setter
    def password(self, password):
        self.password_hash = Bcrypt().generate_password_hash(password).decode('utf-8')

    def check_password(self, password):
        return Bcrypt().check_password_hash(self.password_hash, password)

    @staticmethod
    def check_blacklist(token):
        from app.main.managers.redis_manager import RedisManager
        r = RedisManager().get_redis()
        redis_key = 'BLACKLIST_TOKEN:{token}'.format(token=token)
        blacklisted_at = r.get(redis_key)
        return True if blacklisted_at else False

    @staticmethod
    def encode_auth_token(user_id):
        """
        Generates the Auth Token
        :return: string
        """
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=30, seconds=5),
                'iat': datetime.datetime.utcnow(),
                'sub': user_id
            }
            return jwt.encode(payload, key, algorithm='HS256')
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Decodes the auth token
        :param auth_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(auth_token, key)
            is_blacklisted_token = User.check_blacklist(auth_token)
            if is_blacklisted_token:
                return 'Token blacklisted'
            else:
                return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired'
        except jwt.InvalidTokenError:
            return 'Invalid token'


class UserSecedeLog(db.Model):
    """
    User Secede History
    """
    __tablename__ = "UserSecedeLog"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey('User.id'))
    user = db.relationship('User', backref=db.backref('usersecedelogs', order_by=id))
    reason = db.Column(db.String(100), nullable=False, default='')
    secede_status = db.Column(db.SmallInteger, nullable=False, default=SecedeStatus.APPLIED.value)
    manager = db.Column(db.String(50))
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    handled_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<SecedeLog {},'{}'>".format(self.user_id, self.reason)
