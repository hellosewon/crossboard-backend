from flask import request, g
from flask_restplus import Resource, marshal
from .dto import *
from .services import AuthService
from app.main.util.decorator import http_exception_handler, login_required, check_mobile_verified, marshmallow_validate


@AuthDto.api.route('/login')
@AuthDto.api.response(500, 'Internal server error')
class UserLogin(Resource):

    @http_exception_handler
    @AuthDto.api.expect(AuthDto.login_model, validate=True)
    @marshmallow_validate(LoginSchema.Post, 'json')
    @AuthDto.api.doc(responses={
        200: 'Successfully logged in',
        401: 'Unauthorized',
        400: 'Data validation failed',
        404: 'User not found',
    })
    def post(self):
        """ 로그인 """
        # Setter
        from .models import AccountStatus
        data = request.json
        phone_code = data.get('phone_code')
        phone_number = data.get('phone_number')
        password = data.get('password')

        # Additional Validation
        user = AuthService.get_user(phone_code, phone_number)
        if not user or user.account_status < AccountStatus.NORMAL.value:
            AuthDto.api.abort(404, 'User not found')
        if not user.check_password(password):
            AuthDto.api.abort(401, 'Unauthorized')

        # Action
        auth_token = AuthService.generate_token(user)
        from app.main.user.dto import UserDto
        return {
            'status': 'success',
            'message': 'Successfully logged in',
            'accessToken': auth_token.decode(),
            'userInfo': marshal(user, UserDto.user_read_model),
        }, 200


@AuthDto.api.route('/logout')
@AuthDto.api.response(500, 'Internal server error')
class LogoutAPI(Resource):

    @http_exception_handler
    @AuthDto.api.doc(responses={
        200: 'Successfully logged out',
        401: 'Invalid auth token',
        403: 'Invalid auth token format',
    })
    def post(self):
        """ 로그아웃 """
        # Setter
        auth_header = request.headers.get('Authorization')
        auth_token = auth_header.split(' ')[1] if auth_header else ''

        # Additional Validation
        if not auth_token:
            AuthDto.api.abort(403, 'Auth token is not valid')
        resp = AuthService.decode_auth_token(auth_token)

        # Action
        if not isinstance(resp, str):
            AuthService.save_blacklist_token(auth_token)
        return {
            'status': 'success',
            'message': 'Successfully logged out',
        }, 200


@AuthDto.api.route('/password-reset')
@AuthDto.api.response(500, 'Internal server error')
class PasswordResetAPI(Resource):

    @http_exception_handler
    @AuthDto.api.expect(AuthDto.password_reset_model, validate=True)
    @marshmallow_validate(PasswordResetSchema.Post, 'json')
    @check_mobile_verified('PASSWORD_RESET:VERIFIED')
    @AuthDto.api.doc(responses={
        200: 'Password successfully reset',
        400: 'Data validation failed',
        403: 'Mobile verification code mismatch',
        408: 'Mobile verification code expired',
        404: 'User not found',
    })
    def post(self):
        """ 비밀번호 재설정 """
        # Setter
        data = request.json
        phone_code = data.get('phone_code')
        phone_number = data.get('phone_number')
        password = data.get('password')

        # Additional Validation
        user = AuthService.get_user(phone_code, phone_number)
        if not user:
            AuthDto.api.abort(404, 'User not found')

        # Action
        AuthService.reset_password(user, password)
        return {
            'status': 'success',
            'message': 'Password successfully reset',
        }, 200


@AuthDto.api.route('/password-change')
@AuthDto.api.response(500, 'Internal server error')
class PasswordChangeAPI(Resource):

    @http_exception_handler
    @login_required
    @AuthDto.api.expect(AuthDto.password_change_model, validate=True)
    @marshmallow_validate(PasswordChangeSchema.Put, 'json')
    @AuthDto.api.doc(response={
        200: 'Password successfully changed',
        400: 'Data validation failed',
        401: 'Unauthorized',
    })
    def put(self):
        """ 비밀번호 변경 """
        # Setter
        data = request.json
        password = data.get('password')
        new_password = data.get('new_password')

        user = g.user
        if not user.check_password(password):
            AuthDto.api.abort(401, 'Unauthorized')

        AuthService.change_password(user, new_password)
        return {
            'status': 'success',
            'message': 'Password successfully changed',
        }


@AuthDto.api.route('/phone-change')
@AuthDto.api.response(500, 'Internal server error')
class PhoneChangeAPI(Resource):

    @http_exception_handler
    @login_required
    @AuthDto.api.expect(AuthDto.phone_change_model, validate=True)
    @marshmallow_validate(PhoneChangeSchema.Post, 'json')
    @check_mobile_verified('PHONE_CHANGE:VERIFIED')
    @AuthDto.api.doc(response={
        200: 'Phone successfully changed',
        400: 'Data validation failed',
        401: 'Login required',
        403: 'Mobile verification code mismatch',
        408: 'Mobile verification code expired',
        409: 'User already exists',
    })
    def post(self):
        """ 전화번호 변경 """
        # Setter
        data = request.json
        phone_code = data.get('phone_code')
        phone_number = data.get('phone_number')

        # Additional Validation
        user = AuthService.get_user(phone_code, phone_number)
        if user:
            AuthDto.api.abort(409, 'User already exists')

        # Action
        AuthService.change_phone(g.user, data)
        return {
            'status': 'success',
            'message': 'Phone successfully changed',
        }


@AuthDto.api.route('/mobile-verification')
@AuthDto.api.response(500, 'Internal server error')
class MobileVerificationAPI(Resource):

    @http_exception_handler
    @AuthDto.api.doc(params={
        'phone_code': 'Phone code',
        'phone_number': 'Phone number',
        'purpose': 'Purpose',
    })
    @marshmallow_validate(MobileVerificationSchema.Get, 'args')
    @AuthDto.api.doc(responses={
        202: 'Mobile verification code sent',
        400: 'Data validation failed',
        404: 'User not found',
        409: 'User already exists',
    })
    def get(self):
        """ 모바일 인증번호 요청 """
        # Setter
        phone_code = request.args.get("phone_code")
        phone_number = request.args.get("phone_number")
        purpose = request.args.get("purpose")

        # Additional Validation
        user = AuthService.get_user(phone_code, phone_number)
        if purpose in ('REGISTRATION', 'PHONE_CHANGE') and user:
            return AuthDto.api.abort(409, 'User already exists')
        if purpose == 'PASSWORD_RESET' and not user:
            return AuthDto.api.abort(404, 'User not found')

        # Action
        verification_code = AuthService.generate_mobile_verification_code()
        expire_time = 60 * 10  # 10분
        AuthService.save_mobile_verification_code(phone_code, phone_number, purpose, verification_code, expire_time)
        AuthService.delete_mobile_verification_code(phone_code, phone_number, '{}:VERIFIED'.format(purpose))
        AuthService.send_mobile_verification_code(phone_code, phone_number, verification_code)
        return {
            'status': 'success',
            'message': 'Mobile verification code sent',
            'expire_time': expire_time,
        }, 202

    @http_exception_handler
    @AuthDto.api.expect(AuthDto.mobile_verification_model, validate=True)
    @marshmallow_validate(MobileVerificationSchema.Post, 'json')
    @AuthDto.api.doc(responses={
        200: 'Mobile successfully verified',
        400: 'Data validation failed',
        403: 'Mobile verification code mismatch',
        404: 'User not found',
        408: 'Mobile verification code expired',
        409: 'User already exists',
    })
    def post(self):
        """ 모바일 인증번호 확인 """
        # Setter
        data = request.json
        phone_code = data.get("phone_code")
        phone_number = data.get("phone_number")
        purpose = data.get("purpose")
        verification_code = data.get("verification_code")

        # Additional Validation
        user = AuthService.get_user(phone_code, phone_number)
        if purpose in ('REGISTRATION', 'PHONE_CHANGE') and user:
            return AuthDto.api.abort(409, 'User already exists')
        if purpose == 'PASSWORD_RESET' and not user:
            return AuthDto.api.abort(404, 'User not found')
        saved_code = AuthService.get_mobile_verification_code(phone_code, phone_number, purpose)
        if not saved_code:
            AuthDto.api.abort(408, 'Mobile verification code expired')
        if saved_code != verification_code:
            AuthDto.api.abort(403, 'Mobile verification code mismatch')

        # Action
        AuthService.save_mobile_verification_code(
            phone_code,
            phone_number,
            '{}:VERIFIED'.format(purpose),
            saved_code,
            60 * 10,
        )
        AuthService.delete_mobile_verification_code(phone_code, phone_number, purpose)
        return {
            'status': 'success',
            'message': 'Mobile successfully verified',
        }, 200


@AuthDto.api.route('/ip-address')
@AuthDto.api.response(500, 'Internal server error')
class IPAddressAPI(Resource):

    @http_exception_handler
    def get(self):
        """ IP 주소 가져오기 """
        return {
            'status': 'success',
            'message': 'Get IP Address',
            'ip_address': request.environ.get('HTTP_X_REAL_IP', request.remote_addr),
        }, 200
