from . import db
from .models import User
from app.main.util import trim_phone_number


class AuthService:

    @staticmethod
    def get_user(phone_code, phone_number):
        return User.query.filter_by(phone_code=phone_code, phone_number=trim_phone_number(phone_number)).first()

    @staticmethod
    def generate_token(user):
        try:
            import datetime
            # generate the auth token
            token = user.encode_auth_token(user.id)
            user.last_login_at = datetime.datetime.utcnow()
            db.session.commit()
            return token
        except Exception as e:
            return None

    @staticmethod
    def decode_auth_token(auth_token):
        return User.decode_auth_token(auth_token)

    @staticmethod
    def save_blacklist_token(token):
        import jwt
        import datetime
        from . import key
        from app.main.managers.redis_manager import RedisManager
        r = RedisManager().get_redis()
        redis_key = 'BLACKLIST_TOKEN:{token}'.format(token=token)
        payload = jwt.decode(token, key)
        exp_date = datetime.datetime.utcfromtimestamp(payload['exp'])
        now = datetime.datetime.utcnow()
        expire_time = int((exp_date - now).total_seconds() + 1)
        r.setex(redis_key, expire_time, str(now))

    @staticmethod
    def reset_password(user, new_password):
        user.password = new_password
        db.session.commit()

    @staticmethod
    def change_phone(user, data):
        user.phone_code = data.get('phone_code')
        user.phone_number = trim_phone_number(data.get('phone_number'))
        db.session.commit()

    @staticmethod
    def change_password(user, new_password):
        user.password = new_password
        db.session.commit()

    @staticmethod
    def get_logged_in_user(new_request):
            # get the auth token
            auth_token = new_request.headers.get('Authorization')
            if auth_token:
                resp = User.decode_auth_token(auth_token)
                if not isinstance(resp, str):
                    user = User.query.filter_by(id=resp).first()
                    response_object = {
                        'status': 'success',
                        'data': {
                            'user_id': user.id,
                            'phone_code': user.phone_code,
                            'phone_number': user.phone_number,
                            'is_superuser': user.is_superuser,
                            'registered_at': str(user.registered_at)
                        }
                    }
                    return response_object, 200
                response_object = {
                    'status': 'fail',
                    'message': resp
                }
                return response_object, 401
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'Provide a valid auth token.'
                }
                return response_object, 401

    @staticmethod
    def generate_mobile_verification_code():
        from random import randint
        return str(randint(0, 999999)).zfill(6)

    @staticmethod
    def save_mobile_verification_code(phone_code, phone_number, purpose, value, expire_time):
        from app.main.managers.redis_manager import RedisManager
        r = RedisManager().get_redis()
        redis_key = 'MOBILE_AUTH:{phone_code}:{phone_number}:{purpose}'.format(
            phone_code=phone_code,
            phone_number=trim_phone_number(phone_number),
            purpose=purpose,
        )
        r.setex(redis_key, expire_time, value)

    @staticmethod
    def send_mobile_verification_code(phone_code, phone_number, verification_code):
        from app.main.managers.sms_manager import SMSManager
        SMSManager().request_sms(
            '',
            '{} is the verification code. [CrossBoard]'.format(verification_code),
            phone_code,
            trim_phone_number(phone_number),
        )

    @staticmethod
    def get_mobile_verification_code(phone_code, phone_number, purpose):
        from app.main.managers.redis_manager import RedisManager
        r = RedisManager().get_redis()
        redis_key = 'MOBILE_AUTH:{phone_code}:{phone_number}:{purpose}'.format(
            phone_code=phone_code,
            phone_number=trim_phone_number(phone_number),
            purpose=purpose,
        )
        verification_code = r.get(redis_key)
        return verification_code

    @staticmethod
    def delete_mobile_verification_code(phone_code, phone_number, purpose):
        from app.main.managers.redis_manager import RedisManager
        r = RedisManager().get_redis()
        redis_key = 'MOBILE_AUTH:{phone_code}:{phone_number}:{purpose}'.format(
            phone_code=phone_code,
            phone_number=trim_phone_number(phone_number),
            purpose=purpose,
        )
        return r.delete(redis_key)
