from flask_restplus import Namespace, fields
from marshmallow import Schema, EXCLUDE
from app.main.util.validator import BoardValidator, PostValidator, CommentValidator


# Swagger Document 용
class BoardDto:
    api = Namespace('boards', description='게시판 관련 작업')
    postlabel_model = api.model('board_postlabel_model', {
        'key': fields.String(description='Label key'),
        'name': fields.String(description='Label name'),
    })
    read_model = api.model('board_read_model', {
        'id': fields.Integer(description='Board ID'),
        'board_group': fields.String(description='Board Group'),
        'name': fields.String(description='Name'),
        'url_namespace': fields.String(description='URL Namespace'),
        'board_status': fields.Integer(description='Board status'),
        'post_labels': fields.List(fields.Nested(postlabel_model)),
        'created_at': fields.DateTime(description='Created datetime'),
        'updated_at': fields.DateTime(description='Updated datetime'),
    })
    write_model = api.model('board_write_model', {
        'board_group': fields.String(required=True, description='Board Group'),
        'name': fields.String(required=True, description='Name'),
        'url_namespace': fields.String(required=True, description='URL Namespace'),
        'board_status': fields.Integer(description='Board status'),
    })


class PostDto:
    api = Namespace('posts', description='게시글 관련 작업')
    user_model = api.model('post_user_model', {
        'public_id': fields.String(),
        'nickname': fields.String(),
    })
    read_model = api.model('post_read_model', {
        'id': fields.Integer(description='Post ID'),
        'board_id': fields.Integer(description='Board ID'),
        'is_notice': fields.Boolean(description='Notice?'),
        'is_recommended': fields.Boolean(description='Recommended?'),
        'anon_author': fields.String(description='Anonymous author'),
        'user': fields.Nested(user_model),
        'post_label': fields.Nested(BoardDto.postlabel_model),
        'title': fields.String(description='Title'),
        'content': fields.String(description='Content'),
        'post_status': fields.Integer(description='Post status'),
        'view_count': fields.Integer(description='View count'),
        'created_at': fields.DateTime(description='Created datetime'),
        'updated_at': fields.DateTime(description='Updated datetime'),
        'count_comments': fields.Integer(description='Comment count'),
        'count_likes': fields.Integer(description='Like count'),
        'liked_users': fields.List(fields.Nested(user_model)),
    })
    read_compact_model = api.model('post_read_compact_model', {
        'id': fields.Integer(description='Post ID'),
        'board_id': fields.Integer(description='Board ID'),
        'is_notice': fields.Boolean(description='Notice?'),
        'is_recommended': fields.Boolean(description='Recommended?'),
        'anon_author': fields.String(description='Anonymous author'),
        'user': fields.Nested(user_model),
        'post_label': fields.Nested(BoardDto.postlabel_model),
        'title': fields.String(description='Title'),
        'post_status': fields.Integer(description='Post status'),
        'view_count': fields.Integer(description='View count'),
        'created_at': fields.DateTime(description='Created datetime'),
        'updated_at': fields.DateTime(description='Updated datetime'),
        'count_comments': fields.Integer(description='Comment count'),
        'count_likes': fields.Integer(description='Like count'),
    })
    read_compact_preview_model = api.model('post_read_compact_model', {
        'id': fields.Integer(description='Post ID'),
        'board_id': fields.Integer(description='Board ID'),
        'is_notice': fields.Boolean(description='Notice?'),
        'is_recommended': fields.Boolean(description='Recommended?'),
        'anon_author': fields.String(description='Anonymous author'),
        'user': fields.Nested(user_model),
        'post_label': fields.Nested(BoardDto.postlabel_model),
        'title': fields.String(description='Title'),
        'content': fields.String(description='Content preview'),
        'post_status': fields.Integer(description='Post status'),
        'view_count': fields.Integer(description='View count'),
        'created_at': fields.DateTime(description='Created datetime'),
        'updated_at': fields.DateTime(description='Updated datetime'),
        'count_comments': fields.Integer(description='Comment count'),
        'count_likes': fields.Integer(description='Like count'),
    })
    read_paginated_model = api.model('post_read_paginated_model', {
        'data': fields.List(fields.Nested(read_compact_model)),
        'page_size': fields.Integer(description='Page size'),
        'total_pages': fields.Integer(description='Total pages'),
        'current_page': fields.Integer(description='Current page'),
    })
    read_paginated_preview_model = api.model('post_read_paginated_preview_model', {
        'data': fields.List(fields.Nested(read_compact_preview_model)),
        'page_size': fields.Integer(description='Page size'),
        'total_pages': fields.Integer(description='Total pages'),
        'current_page': fields.Integer(description='Current page'),
    })
    write_normal_model = api.model('post_write_normal_model', {
        'is_notice': fields.Boolean(description='Notice?'),
        'is_recommended': fields.Boolean(description='Recommended?'),
        'anon_author': fields.String(description='Anonymous author'),
        'anon_password': fields.String(description='Anonymous password'),
        'post_label': fields.String(required=True, description='PostLabel key'),
        'title': fields.String(required=True, description='Title'),
        'content': fields.String(required=True, description='Content'),
    })
    write_modify_model = api.model('post_write_modify_model', {
        'post_label': fields.String(required=True, description='PostLabel key'),
        'title': fields.String(required=True, description='Title'),
        'content': fields.String(required=True, description='Content'),
    })


class CommentDto:
    api = Namespace('comments', description='댓글 관련 작업')
    user_model = api.model('comment_user_model', {
        'public_id': fields.String(),
        'nickname': fields.String(),
    })
    read_model = api.model('comment_read_model', {
        'id': fields.Integer(description='Post ID'),
        'post_id': fields.Integer(description='Post ID'),
        'anon_author': fields.String(description='Anonymous author'),
        'user': fields.Nested(user_model),
        'content': fields.String(description='Content'),
        'comment_status': fields.Integer(description='Comment status'),
        'depth': fields.Integer(description='View count'),
        'created_at': fields.DateTime(description='Created datetime'),
        'updated_at': fields.DateTime(description='Updated datetime'),
    })
    write_model = api.model('comment_write_model', {
        'anon_author': fields.String(description='Anonymous author'),
        'anon_password': fields.String(description='Anonymous password'),
        'content': fields.String(required=True, description='Content'),
    })
    write_modify_model = api.model('comment_write_modify_model', {
        'content': fields.String(required=True, description='Content'),
    })


class ImageDto:
    api = Namespace('images', description='이미지 관련 작업')


# 실제 데이터 검증 용
class BoardsSchema:
    class Post(Schema):
        """ (Admin) 새 게시판 생성 """
        board_group = BoardValidator.board_group
        name = BoardValidator.name
        url_namespace = BoardValidator.url_namespace
        board_status = BoardValidator.board_status


class BoardPostsSchema:
    class Get(Schema):
        """ 특정 게시판의 게시글 목록 """
        is_notice = PostValidator.is_notice
        is_recommended = PostValidator.is_recommended
        is_popular = PostValidator.is_popular
        limit = PostValidator.limit
        page = PostValidator.page
        label = PostValidator.label
        q = PostValidator.q

    class Post(Schema):
        """ 특정 게시판에 새 게시글 작성 """
        is_notice = PostValidator.is_notice
        is_recommended = PostValidator.is_recommended
        anon_author = PostValidator.anon_author
        anon_password = PostValidator.anon_password
        post_label = PostValidator.post_label
        title = PostValidator.title
        content = PostValidator.content


class PostsSchema:
    class Put(Schema):
        """ 특정 게시글 수정 """
        post_label = PostValidator.post_label
        title = PostValidator.title
        content = PostValidator.content

        class Meta:
            unknown = EXCLUDE


class PostCommentsSchema:
    class Post(Schema):
        """ 특정 게시글에 댓글 작성 """
        anon_author = CommentValidator.anon_author
        anon_password = CommentValidator.anon_password
        content = CommentValidator.content


class CommentsSchema:
    class Put(Schema):
        """ 특정 댓글 수정 """
        content = CommentValidator.content

        class Meta:
            unknown = EXCLUDE
