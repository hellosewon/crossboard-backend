import datetime
from enum import Enum

from . import db
from flask_bcrypt import Bcrypt


assoc_board_postlabel = db.Table(
    'BoardPostLabel', db.Model.metadata,
    db.Column('board_id', db.Integer, db.ForeignKey('Board.id')),
    db.Column('postlabel_key', db.String(20), db.ForeignKey('PostLabel.key'))
)

assoc_liked_post = db.Table(
    'LikedPost', db.Model.metadata,
    db.Column('user_id', db.Integer, db.ForeignKey('User.id')),
    db.Column('post_id', db.Integer, db.ForeignKey('Post.id'))
)


class PostLabel(db.Model):
    __tablename__ = 'PostLabel'

    key = db.Column(db.String(20), primary_key=True)
    name = db.Column(db.String(50))


class BoardStatus(Enum):
    activated = 1
    unactivated = -1


class Board(db.Model):
    __tablename__ = 'Board'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    board_group = db.Column(db.String(20), nullable=False)
    name = db.Column(db.String(100), nullable=False)
    url_namespace = db.Column(db.String(100), nullable=False)
    board_status = db.Column(db.SmallInteger, nullable=False, default=BoardStatus.activated.value)
    post_labels = db.relationship('PostLabel', secondary=assoc_board_postlabel, backref='boards')
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

    def __repr__(self):
        return '<id:{}, name:{}>'.format(self.id, self.name)


class PostStatus(Enum):
    NORMAL = 1
    DELETED = -1


class Post(db.Model):
    __tablename__ = 'Post'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    board_id = db.Column(db.Integer, db.ForeignKey('Board.id'))
    board = db.relationship('Board', backref=db.backref('posts', order_by=id))
    is_notice = db.Column(db.Boolean, nullable=False, default=False)
    is_recommended = db.Column(db.Boolean, nullable=False, default=False)
    anon_author = db.Column(db.String(100))
    anon_password = db.Column(db.String(100))
    user_id = db.Column(db.Integer, db.ForeignKey('User.id'))
    user = db.relationship('User', backref=db.backref('posts', order_by=id))
    postlabel_key = db.Column(db.String(20), db.ForeignKey('PostLabel.key'))
    post_label = db.relationship('PostLabel', backref=db.backref('posts'))
    title = db.Column(db.String(255), nullable=False)
    content = db.Column(db.Text, nullable=False)
    post_status = db.Column(db.SmallInteger, nullable=False, default=PostStatus.NORMAL.value)
    view_count = db.Column(db.Integer, nullable=False, default=0)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

    liked_users = db.relationship('User', secondary=assoc_liked_post, lazy='dynamic', backref='liked_posts')

    def __repr__(self):
        return '<id:{}, title: {}>'.format(self.id, self.title)

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.anon_password = Bcrypt().generate_password_hash(password).decode('utf-8')

    def check_password(self, password):
        return Bcrypt().check_password_hash(self.anon_password, password)

    @property
    def count_comments(self):
        # TODO: Improve
        return Comment.query.filter_by(post_id=self.id, comment_status=CommentStatus.NORMAL.value).count()

    @property
    def count_likes(self):
        # TODO: Improve
        return self.liked_users.count()


class CommentStatus(Enum):
    NORMAL = 1
    DELETED = -1


class Comment(db.Model):
    __tablename__ = 'Comment'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    post_id = db.Column(db.Integer, db.ForeignKey('Post.id'))
    post = db.relationship('Post', backref=db.backref('comments', order_by=id))
    anon_author = db.Column(db.String(100))
    anon_password = db.Column(db.String(100))
    user_id = db.Column(db.Integer, db.ForeignKey('User.id'))
    user = db.relationship('User', backref=db.backref('comments', order_by=id))
    content = db.Column(db.Text, nullable=False)
    comment_status = db.Column(db.SmallInteger, nullable=False, default=CommentStatus.NORMAL.value)
    depth = db.Column(db.Integer, nullable=False, default=0)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

    def __repr__(self):
        return '<id:{}, content: {}>'.format(self.id, self.content[:20])

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.anon_password = Bcrypt().generate_password_hash(password).decode('utf-8')

    def check_password(self, password):
        return Bcrypt().check_password_hash(self.anon_password, password)

