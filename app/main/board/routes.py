from flask import request, g
from flask_restplus import Resource
from .dto import *
from .services import *
from app.main.util.paginator import Paginator
from app.main.util.decorator import http_exception_handler, login_required, marshmallow_validate


@BoardDto.api.route('')
@BoardDto.api.response(500, 'Internal server error')
class Boards(Resource):

    @http_exception_handler
    @BoardDto.api.marshal_list_with(BoardDto.read_model, envelope='data')
    @BoardDto.api.doc(params={
        'board_group': 'Board group',
    })
    def get(self):
        """ 게시판 목록 """
        board_group = request.args.get("board_group")
        return BoardService.get_boards(board_group)

    @http_exception_handler
    @BoardDto.api.expect(BoardDto.write_model, validate=True)
    @marshmallow_validate(BoardsSchema.Post, 'json')
    @BoardDto.api.doc(responses={
        201: 'Board successfully created',
        400: 'Data validation failed',
        409: 'Duplicate URL namespace',
    })
    def post(self):
        """ (Admin) 새 게시판 생성 """
        # Setter
        data = request.json
        url_namespace = data['url_namespace']

        # Additional Validation
        board = BoardService.get_board_by_url_namespace(url_namespace)
        if board:
            BoardDto.api.abort(409, 'Duplicate URL namespace')

        # Action
        BoardService.create_new_board(data=data)
        return {
            'status': 'success',
            'message': 'Board successfully created',
        }, 201


@BoardDto.api.route('/<board_id>')
@BoardDto.api.param('board_id', 'Board ID')
@BoardDto.api.response(500, 'Internal server error')
class Boards(Resource):

    @http_exception_handler
    @BoardDto.api.marshal_with(BoardDto.read_model)
    @BoardDto.api.doc(responses={
        404: 'Board not found',
    })
    def get(self, board_id):
        """ 특정 게시판의 상세정보 """
        board = BoardService.get_board(board_id)
        if not board:
            BoardDto.api.abort(404, 'Board not found')
        return board


@BoardDto.api.route('/<board_id>/posts')
@BoardDto.api.param('board_id', 'Board ID')
@BoardDto.api.response(500, 'Internal server error')
class BoardPosts(Resource):

    @http_exception_handler
    @marshmallow_validate(BoardPostsSchema.Get, 'args')
    @BoardDto.api.doc(params={
        'is_notice': 'Notice?',
        'is_recommended': 'Recommended?',
        'is_popular': 'Popular?',
        'limit': 'Limit',
        'page': 'Page',
        'label': 'Post label',
        'q': 'Search query',
    })
    @BoardDto.api.doc(responses={
        400: 'Data validation failed',
        404: 'Board not found',
    })
    def get(self, board_id):
        """ 특정 게시판의 게시글 목록 """
        # Setter
        data = request.args

        # Additional Validation
        board = BoardService.get_board(board_id)
        if not board:
            BoardDto.api.abort(404, 'Board not found')

        # Action
        posts = BoardService.get_board_posts(board_id, data)
        result = Paginator().gen_response(posts, current_page=int(data.get('page', 1)))
        model = PostDto.read_paginated_preview_model if data.get('is_recommended') == '1' else PostDto.read_paginated_model
        return BoardDto.api.marshal(result, model), 200

    @http_exception_handler
    @BoardDto.api.expect(PostDto.write_normal_model, validate=True)
    @marshmallow_validate(BoardPostsSchema.Post, 'json')
    @BoardDto.api.doc(responses={
        201: 'Post successfully created',
        400: 'Data validation failed',
        404: 'Board not found',
    })
    def post(self, board_id):
        """ 특정 게시판에 새 게시글 작성 """
        # TODO: Board read/write permission check
        # Setter
        data = request.json

        # Additional Validation
        board = BoardService.get_board(board_id)
        if not board:
            BoardDto.api.abort(404, 'Board not found.')

        # Action
        BoardService.create_new_board_post(board_id, g.user, data)
        return {
            'status': 'success',
            'message': 'Post successfully created',
        }, 201


@PostDto.api.route('/<post_id>')
@PostDto.api.param('post_id', 'Post ID')
@PostDto.api.response(500, 'Internal server error')
class Posts(Resource):

    @http_exception_handler
    @PostDto.api.marshal_list_with(PostDto.read_model, envelope='data')
    @PostDto.api.doc(responses={
        404: 'Post not found',
    })
    def get(self, post_id):
        """ 특정 게시글 상세정보 """
        post = PostService.get_a_post(post_id)
        if not post or post.post_status == -1:
            PostDto.api.abort(404, 'Post not found')
        PostService.increase_post_view_count(post)
        return post

    @http_exception_handler
    @PostDto.api.expect(PostDto.write_modify_model, validate=True)
    @marshmallow_validate(PostsSchema.Put, 'json')
    @PostDto.api.doc(responses={
        200: 'Post successfully modified',
        400: 'Data validation failed',
        403: 'Permission denied',
        404: 'Post not found',
    })
    def put(self, post_id):
        """ 특정 게시글 수정 """
        # Setter
        data = request.json

        # Addtional Validation
        post = PostService.get_a_post(post_id)
        if not post:
            PostDto.api.abort(404, 'Post not found')
        if not PostService.is_post_owner(post, g.user, request.json):
            PostDto.api.abort(403, 'Permission denied')

        # Action
        PostService.modify_a_post(post, data=data)
        return {
            'status': 'success',
            'message': 'Post successfully modified',
        }, 200

    @http_exception_handler
    @PostDto.api.doc(responses={
        200: 'Post successfully deleted',
        403: 'Permission denied',
        404: 'Post not found',
    })
    def delete(self, post_id):
        """ 특정 게시글 삭제 """
        post = PostService.get_a_post(post_id)
        if not post:
            PostDto.api.abort(404, 'Post not found')
        if not PostService.is_post_owner(post, g.user, request.json):
            PostDto.api.abort(403, 'Permission denied')
        PostService.delete_a_post(post)
        return {
            'status': 'success',
            'message': 'Post successfully deleted',
        }, 200


@PostDto.api.route('/<post_id>/comments')
@PostDto.api.param('post_id', 'Post ID')
@PostDto.api.response(500, 'Internal server error')
class PostComments(Resource):

    @http_exception_handler
    @PostDto.api.marshal_list_with(CommentDto.read_model, envelope='data')
    @PostDto.api.doc(responses={
        404: 'Post not found',
    })
    def get(self, post_id):
        """ 특정 게시글 댓글 목록 """
        post = PostService.get_a_post(post_id)
        if not post:
            PostDto.api.abort(404, 'Post not found')
        return PostService.get_post_comments(post_id)

    @http_exception_handler
    @PostDto.api.expect(CommentDto.write_model, validate=True)
    @marshmallow_validate(PostCommentsSchema.Post, 'json')
    @PostDto.api.doc(responses={
        201: 'Comment successfully created',
        400: 'Data validation failed',
        404: 'Post not found',
    })
    def post(self, post_id):
        """ 특정 게시글에 댓글 작성 """
        # TODO: Board read/write permission check
        # Setter
        data = request.json

        # Additional Validation
        post = PostService.get_a_post(post_id)
        if not post:
            PostDto.api.abort(404, 'Post not found')

        # Action
        PostService.create_new_post_comment(post_id, g.user, data=data)
        return {
            'status': 'success',
            'message': 'Comment successfully created',
        }, 201


@CommentDto.api.route('/<comment_id>')
@CommentDto.api.param('comment_id', 'Comment ID')
@CommentDto.api.response(500, 'Internal server error')
class Comments(Resource):

    @http_exception_handler
    @CommentDto.api.marshal_list_with(CommentDto.read_model, envelope='data')
    @CommentDto.api.doc(responses={
        404: 'Comment not found',
    })
    def get(self, comment_id):
        """ 특정 댓글 상세정보 """
        comment = CommentService.get_a_comment(comment_id)
        if not comment or comment.comment_status == -1:
            CommentDto.api.abort(404, 'Comment not found')
        return comment

    @http_exception_handler
    @CommentDto.api.expect(CommentDto.write_modify_model, validate=True)
    @marshmallow_validate(CommentsSchema.Put, 'json')
    @CommentDto.api.doc(responses={
        200: 'Comment successfully modified',
        400: 'Data validation failed',
        403: 'Permission denied',
        404: 'Comment not found',
    })
    def put(self, comment_id):
        """ 특정 댓글 수정 """
        # Setter
        data = request.json

        # Additional Validation
        comment = CommentService.get_a_comment(comment_id)
        if not comment:
            CommentDto.api.abort(404, 'Comment not found')
        if not CommentService.is_comment_owner(comment, g.user, data):
            CommentDto.api.abort(403, 'Permission denied')

        # Action
        CommentService.modify_a_comment(comment, data=data)
        return {
            'status': 'success',
            'message': 'Comment successfully modified',
        }, 200

    @http_exception_handler
    @CommentDto.api.doc(responses={
        200: 'Comment successfully deleted',
        403: 'Permission denied',
        404: 'Comment not found',
    })
    def delete(self, comment_id):
        """ 특정 댓글 삭제 """
        comment = CommentService.get_a_comment(comment_id)
        if not comment:
            CommentDto.api.abort(404, 'Comment not found')
        if not CommentService.is_comment_owner(comment, g.user, request.json):
            CommentDto.api.abort(403, 'Permission denied')
        CommentService.delete_a_comment(comment)
        return {
            'status': 'success',
            'message': 'Comment successfully deleted',
        }, 200


@PostDto.api.route('/<post_id>/anon-password-check')
@PostDto.api.param('post_id', 'Post ID')
@PostDto.api.response(500, 'Internal server error')
class AnonPostPasswordCheck(Resource):

    @http_exception_handler
    @PostDto.api.doc(responses={
        200: 'Anon Password successfully checked',
        403: 'Permission denied',
        404: 'Post not found',
    })
    def post(self, post_id):
        """ 비로그인 유저의 게시글 패스워드 체크 """
        post = PostService.get_a_post(post_id)
        if not post:
            PostDto.api.abort(404, 'Post not found')
        if not PostService.is_post_owner(post, g.user, request.json):
            PostDto.api.abort(403, 'Permission denied')
        return {
            'status': 'success',
            'message': 'Anon Password successfully checked',
        }, 200


@CommentDto.api.route('/<comment_id>/anon-password-check')
@CommentDto.api.param('comment_id', 'Comment ID')
@CommentDto.api.response(500, 'Internal server error')
class AnonCommentPasswordCheck(Resource):

    @http_exception_handler
    @CommentDto.api.doc(responses={
        200: 'Anon Password successfully checked',
        403: 'Permission denied',
        404: 'Comment not found',
    })
    def post(self, comment_id):
        """ 비로그인 유저의 코멘트 패스워드 체크 """
        comment = CommentService.get_a_comment(comment_id)
        if not comment or comment.comment_status == -1:
            CommentDto.api.abort(404, 'Comment not found')
        if not CommentService.is_comment_owner(comment, g.user, request.json):
            CommentDto.api.abort(403, 'Permission denied')
        return {
            'status': 'success',
            'message': 'Anon Password successfully checked',
        }, 200


@PostDto.api.route('/<post_id>/liked-users')
@PostDto.api.param('post_id', 'Post ID')
@PostDto.api.response(500, 'Internal server error')
class PostLikedUsers(Resource):

    @http_exception_handler
    @login_required
    @PostDto.api.doc(responses={
        201: 'Post successfully liked',
        401: 'Login required',
        404: 'Post not found',
        409: 'Post already liked',
    })
    def post(self, post_id):
        """ 특정 게시글 좋아요 """
        post = PostService.get_a_post(post_id)
        if not post:
            PostDto.api.abort(404, 'Post not found')
        exists = post.liked_users.filter_by(id=g.user.id).first()
        if exists:
            PostDto.api.abort(409, 'Post already liked')
        PostService.create_new_liked_post(post, g.user)
        return {
            'status': 'success',
            'message': 'Successfully created.',
        }, 201

    @http_exception_handler
    @login_required
    @PostDto.api.doc(responses={
        200: 'Post successfully un-liked',
        401: 'Login required',
        404: 'Post not found',
        409: 'Post already un-liked',
    })
    def delete(self, post_id):
        """ 특정 게시글 좋아요 취소 """
        post = PostService.get_a_post(post_id)
        if not post:
            PostDto.api.abort(404, 'Post not found')
        exists = post.liked_users.filter_by(id=g.user.id).first()
        if not exists:
            PostDto.api.abort(409, 'Post already un-liked')
        PostService.delete_liked_post(post, g.user)
        return {
            'status': 'success',
            'message': 'Post successfully un-liked',
        }, 200


@ImageDto.api.route('')
@ImageDto.api.response(500, 'Internal server error')
class Images(Resource):

    @http_exception_handler
    @ImageDto.api.doc(responses={
        201: 'Board successfully created',
        400: 'Data validation failed',
        409: 'Duplicate URL namespace',
    })
    def post(self):
        """ 이미지 업로드 """
        # Setter
        f = request.files['file']

        # Action
        image_url = ImageService.upload_image(f)
        return {
            'status': 'success',
            'message': 'Board successfully created',
            'image_url': image_url,
        }, 201
