from . import db
from .models import Board, Post, Comment, assoc_liked_post, CommentStatus, BoardStatus
import datetime


class BoardService:

    @staticmethod
    def create_new_board(data):
        new_board = Board(
            board_group=data['board_group'],
            name=data['name'],
            url_namespace=data['url_namespace'],
            board_status=data.get('board_status', BoardStatus.activated.value),
        )
        BoardService.save_changes(new_board)
        return True

    @staticmethod
    def get_boards(board_group):
        if not board_group:
            return Board.query.filter_by(board_status=BoardStatus.activated.value).all()
        return Board.query.filter_by(board_group=board_group, board_status=BoardStatus.activated.value).all()

    @staticmethod
    def get_board(board_id):
        return Board.query.filter_by(id=board_id).first()

    @staticmethod
    def get_board_by_url_namespace(url_namespace):
        return Board.query.filter_by(url_namespace=url_namespace).first()

    @staticmethod
    def get_board_posts(board_id, args):
        filters = dict()
        if args.get('is_notice') == '1':
            filters['is_notice'] = True
        if args.get('is_recommended') == '1':
            filters['is_recommended'] = True
        if args.get('label'):
            filters['postlabel_key'] = args.get('label')
        posts = Post.query.filter_by(board_id=board_id).filter_by(**filters)

        # 검색 쿼리 있을 경우 TODO: 유저 닉네임 검색 안됨
        if args.get('q'):
            q_set = args.get('q').split(',')
            for q in q_set:
                k, v = q.split('=')
                posts = posts.filter(getattr(Post, k).like('%{}%'.format(v)))

        if args.get('is_popular') == '1':
            # 각 게시판의 글 생성일이 최근 2주 이내, 추천이 많은 순서대로, 5개의 게시물 표시 (추천 수가 0인 것은 제외)
            from dateutil.relativedelta import relativedelta
            start_time = datetime.datetime.utcnow() - relativedelta(days=14)
            posts = posts.filter(Post.created_at >= start_time)
            posts = posts.outerjoin(assoc_liked_post).group_by(Post.id)\
                .order_by(db.func.count(assoc_liked_post.c.user_id).desc(), Post.view_count.desc(), Post.id.desc())\
                .having(db.func.count(assoc_liked_post.c.user_id) > 0)
        else:
            posts = posts.order_by(Post.id.desc())
        if args.get('limit'):
            posts = posts.limit(int(args.get('limit')))
        return posts.all()

    @staticmethod
    def create_new_board_post(board_id, user, data):
        new_post = Post(
            board_id=board_id,
            is_notice=data.get('is_notice', False),
            is_recommended=data.get('is_recommended', False),
            postlabel_key=data.get('post_label'),
            title=data['title'],
            content=data['content'],
        )
        if user:
            new_post.user = user
        else:
            new_post.anon_author = data['anon_author']
            new_post.password = data['anon_password']
        BoardService.save_changes(new_post)

    @staticmethod
    def save_changes(data):
        db.session.add(data)
        db.session.commit()


class PostService:

    @staticmethod
    def is_post_owner(post, user, data):
        if user and user.is_superuser:
            return True
        if user and post.user_id:
            return user.id == post.user_id
        elif not post.user_id:
            return post.check_password(data.get('anon_password'))
        else:
            return False

    @staticmethod
    def get_a_post(post_id):
        return Post.query.filter_by(id=post_id).first()

    @staticmethod
    def increase_post_view_count(post):
        post.view_count += 1
        db.session.commit()

    @staticmethod
    def modify_a_post(post, data):
        post.postlabel_key = data.get('post_label')
        post.title = data.get('title')
        post.content = data.get('content')
        post.updated_at = datetime.datetime.utcnow()
        db.session.commit()

    @staticmethod
    def delete_a_post(post):
        post.post_status = -1
        db.session.commit()

    @staticmethod
    def get_post_comments(post_id):
        return Comment.query.filter_by(post_id=post_id,
                                       comment_status=CommentStatus.NORMAL.value).order_by(Comment.id.desc()).all()

    @staticmethod
    def create_new_post_comment(post_id, user, data):
        new_comment = Comment(
            post_id=post_id,
            content=data['content'],
        )
        if user:
            new_comment.user = user
        else:
            new_comment.anon_author = data['anon_author']
            new_comment.password = data['anon_password']
        PostService.save_changes(new_comment)

    @staticmethod
    def create_new_liked_post(post, user):
        post.liked_users.append(user)
        PostService.save_changes(post)

    @staticmethod
    def delete_liked_post(post, user):
        post.liked_users.remove(user)
        db.session.commit()

    @staticmethod
    def save_changes(data):
        db.session.add(data)
        db.session.commit()


class CommentService:

    @staticmethod
    def is_comment_owner(comment, user, data):
        if user and user.is_superuser:
            return True
        if user and comment.user_id:
            return user.id == comment.user_id
        elif not comment.user_id:
            return comment.check_password(data.get('password'))
        else:
            return False

    @staticmethod
    def get_a_comment(comment_id):
        return Comment.query.filter_by(id=comment_id).first()

    @staticmethod
    def modify_a_comment(comment, data):
        comment.content = data.get('content')
        comment.updated_at = datetime.datetime.utcnow()
        db.session.commit()

    @staticmethod
    def delete_a_comment(comment):
        comment.comment_status = -1
        db.session.commit()


class ImageService:

    @staticmethod
    def upload_image(image_file, acl='public-read'):
        import boto3
        from manage import app
        import time
        file_name = str(int(time.time()*1000))
        s3 = boto3.client(
            "s3",
            aws_access_key_id=app.config['S3_KEY'],
            aws_secret_access_key=app.config['S3_SECRET'],
        )
        s3.upload_fileobj(
            image_file,
            app.config['S3_BUCKET'],
            file_name,
            ExtraArgs={
                'ACL': acl,
                'ContentType': image_file.content_type
            }
        )
        return 'http://{}.s3.amazonaws.com/{}'.format(app.config['S3_BUCKET'], file_name)
