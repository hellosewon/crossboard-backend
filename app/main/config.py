import os

# uncomment the line below for postgres database url from environment variable
# postgres_local_base = os.environ['DATABASE_URL']

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SECRET_KEY = os.getenv('SECRET_KEY', '_7&%-vewfuwc7=3&qj77j*wy8%=lcjg@rt1itde!f(c($k-cl0')
    DEBUG = False
    ERROR_404_HELP = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ALARM_SERVER_URL = 'http://devalarm.crosswon.com/alarm/'
    S3_BUCKET = 's3cross-board-media-dev'
    S3_KEY = 'AKIA4WKRN6TFWL4U2K4F'
    S3_SECRET = '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'


class TestConfig(Config):
    DEBUG = True
    ENV = 'test'
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://cbappdev:1234@localhost:5432/crossboard_test'
    REDIS_HOST, REDIS_PORT, REDIS_PASS = "127.0.0.1", 6379, ""


class LocalConfig(Config):
    DEBUG = True
    ENV = 'local'
    SQLALCHEMY_DATABASE_URI = 'postgresql://cbappdev:1234@localhost:5432/crossboard_dev'
    REDIS_HOST, REDIS_PORT, REDIS_PASS = "127.0.0.1", 6379, ""


class DevConfig(Config):
    DEBUG = True
    ENV = 'dev'
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:zmfhtmWkd17!@dev-board-db.csvquotv1kaf.ap-northeast-2.rds.amazonaws.com:5432/crossboard_dev'
    REDIS_HOST, REDIS_PORT, REDIS_PASS = "dev-redis.xd78es.ng.0001.apn2.cache.amazonaws.com", 6379, ""


class RealConfig(Config):
    DEBUG = False
    ENV = 'real'
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:zmfhtmWkd17!@prdt-board-db.csvquotv1kaf.ap-northeast-2.rds.amazonaws.com:5432/crossboard_real'
    REDIS_HOST, REDIS_PORT, REDIS_PASS = "prdt-board-redis.xd78es.ng.0001.apn2.cache.amazonaws.com", 6379, ""
    ALARM_SERVER_URL = 'https://alarm.crossenf.com/alarm/'
    S3_BUCKET = 's3cross-board-media-real'


config_by_name = dict(
    test=TestConfig,
    local=LocalConfig,
    dev=DevConfig,
    real=RealConfig
)

key = Config.SECRET_KEY
