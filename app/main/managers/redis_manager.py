import redis
from manage import app


class RedisManager:

    @staticmethod
    def get_redis():
        pool = redis.ConnectionPool(
            host=app.config['REDIS_HOST'],
            port=app.config['REDIS_PORT'],
            password=app.config['REDIS_PASS'],
            decode_responses=True,
        )
        return redis.StrictRedis(connection_pool=pool)
