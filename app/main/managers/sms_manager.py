from app.main.util import request_to_alarm


class SMSManager:

    provider = 'Infobank'

    def request_sms(self, title, text, phone_code, phone_number):
        data = {'title': title, 'text': text}
        targets = ['{},{}'.format(phone_code, phone_number)]
        request_to_alarm(
            alarm_type="{provider} {lang}".format(provider=self.provider, lang='en'),
            data=data,
            targets=targets,
            manager='crossboard-backend',
            memo=None,
        )
