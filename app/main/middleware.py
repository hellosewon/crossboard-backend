from flask import request, g
from app.main.auth.models import User


class RequestMiddleware:

    @staticmethod
    def init_app(app):

        # @app.before_first_request
        # def before_first_request_func():
        #     """
        #     This function will run once before the first request to this instance of the application.
        #     You may want to use this function to create any databases/tables required for your app.
        #     """
        #     pass
        #     print("This function will run once ")

        @app.before_request
        def before_request_func():
            """
            This function will run before every request. Let's add something to the session & g.
            It's a good place for things like establishing database connections, retrieving
            user information from the session, assigning values to the flask g object etc..
            We have access to the request context.
            """
            # get the auth token
            g.user = None
            auth_header = request.headers.get('Authorization')
            g.lang = request.headers.get('Language', 'en')
            if auth_header:
                try:
                    auth_token = auth_header.split(" ")[1]
                except IndexError:
                    return {
                        'status': 'fail',
                        'message': 'Bearer token malformed.'
                    }, 401
            else:
                auth_token = ''

            if auth_token:
                decode_result = User.decode_auth_token(auth_token)
                if isinstance(decode_result, str):
                    return {
                        'status': 'fail',
                        'message': decode_result,
                    }, 401
                g.user = User.query.filter_by(id=decode_result).first()
                if request.method != 'OPTIONS' and g.user and g.user.lang != g.lang:
                    from . import db
                    g.user.lang = g.lang
                    db.session.commit()

        # @app.after_request
        # def after_request_func(response):
        #     """
        #     This function will run after a request, as long as no exceptions occur.
        #     It must take and return the same parameter - an instance of response_class.
        #     This is a good place to do some application cleanup.
        #     """
        #     pass
        #     username = g.username
        #     foo = session.get("foo")
        #     print("after_request is running!", username, foo)
        #     return response

        # @app.teardown_request
        # def teardown_request_func(error=None):
        #     """
        #     This function will run after a request, regardless if an exception occurs or not.
        #     It's a good place to do some cleanup, such as closing any database connections.
        #     If an exception is raised, it will be passed to the function.
        #     You should so everything in your power to ensure this function does not fail, so
        #     liberal use of try/except blocks is recommended.
        #     """
        #     print("teardown_request is running!")
        #     if error:
        #         # Log the error
        #         print(str(error))
