from flask_restplus import Namespace, fields
from marshmallow import Schema
from app.main.util.validator import AuthValidator, UserValidator


# Swagger Document 용
class UserDto:
    api = Namespace('users', description='회원 관련 작업')
    user_read_model = api.model('user', {
        'public_id': fields.String(description='User public ID'),
        'phone_code': fields.String(description='Phone code'),
        'phone_number': fields.String(description='Phone number'),
        'password': fields.String(description='Password'),
        'is_superuser': fields.Boolean(description='Superuser?'),
        'nickname': fields.String(description='Nickname'),
        'lang': fields.String(description='User language'),
        'preferred_country': fields.String(description='Preferred country'),
        'agree_terms': fields.Boolean(description='Agreed to terms?'),
        'agree_privacy': fields.Boolean(description='Agreed to privacy?'),
        'agree_sms_notice': fields.Boolean(description='Agreed to sms notice?'),
        'agree_sms_marketing': fields.Boolean(description='Agreed to sms marketing?'),
        'count_comments': fields.Integer(description='Comments count'),
        'count_posts': fields.Integer(description='Posts count'),
    })
    user_write_model = api.model('user', {
        'phone_code': fields.String(required=True, description='Phone code'),
        'phone_number': fields.String(required=True, description='Phone number'),
        'password': fields.String(required=True, description='Password'),
        'nickname': fields.String(required=True, description='Nickname'),
        'lang': fields.String(required=True, description='User language'),
        'preferred_country': fields.String(required=True, description='Preferred country'),
        'agree_terms': fields.Boolean(required=True, description='Agreed to terms?'),
        'agree_privacy': fields.Boolean(required=True, description='Agreed to privacy?'),
        'agree_sms_notice': fields.Boolean(required=True, description='Agreed to sms notice?'),
        'agree_sms_marketing': fields.Boolean(required=True, description='Agreed to sms marketing?'),
        'verification_code': fields.String(required=True, description='Verification code'),
    })
    secede_read_model = api.model('secede', {
        'id': fields.Integer(description='ID'),
        'user_id': fields.Integer(description='User ID'),
        'reason': fields.String(desciption='Reason'),
        'secede_status': fields.Integer(desciption='Secede status'),
        'manager': fields.String(desciption='Manager'),
        'created_at': fields.DateTime(description='Created datetime'),
        'handled_at': fields.DateTime(description='Handled datetime'),
    })
    secede_paginated_model = api.model('secede_read_paginated_model', {
        'data': fields.List(fields.Nested(secede_read_model)),
        'page_size': fields.Integer(description='Page size'),
        'total_pages': fields.Integer(description='Total pages'),
        'current_page': fields.Integer(description='Current page'),
    })
    user_admin_read_model = api.model('user_admin', {
        'id': fields.String(description='User ID'),
        'public_id': fields.String(description='User public ID'),
        'phone_code': fields.String(description='Phone code'),
        'phone_number': fields.String(description='Phone number'),
        'is_superuser': fields.Boolean(description='Superuser?'),
        'nickname': fields.String(description='Nickname'),
        'lang': fields.String(description='User language'),
        'preferred_country': fields.String(description='Preferred country'),
        'count_comments': fields.Integer(description='Comments count'),
        'count_posts': fields.Integer(description='Posts count'),
        'account_status': fields.Integer(description='Account status'),
        'registered_at': fields.DateTime(description='Registered datetime'),
        'last_login_at': fields.DateTime(description='Last login datetime'),
    })
    user_admin_paginated_model = api.model('user_admin_paginated_model', {
        'data': fields.List(fields.Nested(user_admin_read_model)),
        'page_size': fields.Integer(description='Page size'),
        'total_pages': fields.Integer(description='Total pages'),
        'current_page': fields.Integer(description='Current page'),
    })


# 실제 데이터 검증 용
class UserListSchema:
    class Post(Schema):
        """ 회원가입 """
        phone_code = AuthValidator.phone_code
        phone_number = AuthValidator.phone_number
        password = AuthValidator.password
        nickname = UserValidator.nickname
        lang = UserValidator.lang
        preferred_country = UserValidator.preferred_country
        agree_terms = UserValidator.agree_terms
        agree_privacy = UserValidator.agree_privacy
        agree_sms_notice = UserValidator.agree_sms_notice
        agree_sms_marketing = UserValidator.agree_sms_marketing
        verification_code = AuthValidator.mobile_verification_code
