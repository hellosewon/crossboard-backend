from flask import request, g
from flask_restplus import Resource, marshal
from .dto import *
from .services import UserService
from app.main.util.paginator import Paginator
from app.main.util.decorator import http_exception_handler, check_mobile_verified, marshmallow_validate,\
    login_required, admin_required
from ..auth.models import AccountStatus


@UserDto.api.route('')
@UserDto.api.response(500, 'Internal server error')
class UserList(Resource):

    @http_exception_handler
    @admin_required
    @UserDto.api.doc(params={
        'page': 'Page',
        'page_size': 'Page size',
        'q': 'Search query',
    })
    def get(self):
        """ (Admin) 모든 회원 목록 """
        # Setter
        data = request.args

        # Additional Validation

        # Action
        users = UserService.get_all_users(data)
        result = Paginator(
            page_size=int(data.get('page_size', 10))
        ).gen_response(users, current_page=int(data.get('page', 1)))
        model = UserDto.user_admin_paginated_model
        return UserDto.api.marshal(result, model), 200

    @http_exception_handler
    @UserDto.api.expect(UserDto.user_write_model, validate=True)
    @marshmallow_validate(UserListSchema.Post, 'json')
    @check_mobile_verified('REGISTRATION:VERIFIED')
    @UserDto.api.doc(responses={
        201: 'User successfully created',
        400: 'Data validation failed',
        401: 'Error generating token',
        403: 'Mobile verification code mismatch',
        408: 'Mobile verification code expired',
        409: 'User already exists',
    })
    def post(self):
        """ 회원가입 """
        # Setter
        data = request.json
        phone_code = data.get('phone_code')
        phone_number = data.get('phone_number')

        # Additional Validation
        user = UserService.get_user(phone_code, phone_number)
        if user:
            UserDto.api.abort(409, 'User already exists')

        # Action
        data['lang'] = g.lang
        new_user = UserService.create_new_user(data=data)
        auth_token = UserService.generate_token(new_user)
        if not auth_token:
            UserDto.api.abort(401, 'Error generating token')
        return {
            'status': 'success',
            'message': 'User successfully created',
            'accessToken': auth_token.decode(),
            'userInfo': marshal(new_user, UserDto.user_read_model),
        }, 201


@UserDto.api.route('/<public_id>')
@UserDto.api.param('public_id', 'User identifier')
@UserDto.api.response(500, 'Internal server error')
class User(Resource):

    @http_exception_handler
    @UserDto.api.marshal_with(UserDto.user_read_model)
    @UserDto.api.doc(responses={
        400: 'Data validation failed',
        404: 'User not found',
    })
    def get(self, public_id):
        """ 특정 회원 상세정보 """
        if not public_id:
            UserDto.api.abort(400, 'Data validation failed')
        if public_id == 'me':
            return g.user
        user = UserService.get_user_by_public_id(public_id)
        if not user:
            UserDto.api.abort(404, 'User not found')
        return user

    @http_exception_handler
    @admin_required
    @UserDto.api.doc(response={
        200: 'User successfully handled',
        400: 'Data validation failed',
        401: 'Admin required',
    })
    def patch(self, public_id):
        """ (Admin) 관리자 등록/취소 하기 """
        # Setter
        data = request.json

        # Additional Validation
        accepting_keys = ['is_superuser']
        if set(accepting_keys).isdisjoint(data.keys()):
            UserDto.api.abort(400, 'Data validation failed')

        # Action
        UserService.handle_user(data, public_id)
        return {
            'status': 'success',
            'message': 'User successfully handled',
        }, 200


@UserDto.api.route('/preferred-country-change')
@UserDto.api.response(500, 'Internal server error')
class PreferredCountryChangeAPI(Resource):

    @http_exception_handler
    @login_required
    @UserDto.api.doc(response={
        200: 'Preferred Country successfully changed',
        400: 'Data validation failed',
        401: 'Login required',
    })
    def put(self):
        """ 관심국가 변경 """
        # Setter
        data = request.json
        # Action
        UserService.change_preferred_country(g.user, data)
        return {
            'status': 'success',
            'message': 'Preferred Country successfully changed',
        }, 200


@UserDto.api.route('/secede-application')
@UserDto.api.response(500, 'Internal server error')
class SecedeLogs(Resource):

    @http_exception_handler
    @admin_required
    @UserDto.api.doc(params={
        'page': 'Page',
        'page_size': 'Page size',
        'q': 'Search query',
    })
    def get(self):
        """ (Admin) 탈퇴 요청 로그 가져오기 """
        # Setter
        data = request.args

        # Additional Validation

        # Action
        secede_logs = UserService.get_secede_logs(data)
        result = Paginator(
            page_size=int(data.get('page_size', 10))
        ).gen_response(secede_logs, current_page=int(data.get('page', 1)))
        model = UserDto.secede_paginated_model
        return UserDto.api.marshal(result, model), 200

    @http_exception_handler
    @login_required
    @UserDto.api.doc(response={
        200: 'User Secede successfully applied',
        401: 'Unauthorized',
        404: 'User not found',
    })
    def post(self):
        """ User 탈퇴신청 """
        # Setter
        data = request.json
        password = data.get('password')
        user = g.user
        if not user.check_password(password):
            UserDto.api.abort(401, 'Unauthorized')
        if user.account_status == AccountStatus.SECEDE_APPLIED.value:
            UserDto.api.abort(410, 'Secede Application is Processing')
        if user.account_status == AccountStatus.SECEDED_COMPLETED.value:
            UserDto.api.abort(404, 'User not found')

        # Action
        reason = data.get('secedeReason')
        if reason == 'ETC':
            reason = data.get('secedeReasonDesc')
        UserService.secede_apply(user, reason)
        return {
            'status': 'success',
            'message': 'User Secede successfully applied',
        }, 200


@UserDto.api.route('/secede-application/<id>')
@UserDto.api.param('id', 'ID')
@UserDto.api.response(500, 'Internal server error')
class SecedeLog(Resource):

    @http_exception_handler
    @admin_required
    @UserDto.api.doc(response={
        200: 'Secede log successfully handled',
        400: 'Data validation failed',
        401: 'Admin required',
    })
    def patch(self, id):
        """ (Admin) 탈퇴 요청 승인/거절 하기 """
        # Setter
        data = request.json

        # Additional Validation
        accepting_keys = ['secede_status']
        if set(accepting_keys).isdisjoint(data.keys()):
            UserDto.api.abort(400, 'Data validation failed')

        # Action
        UserService.handle_secede_log(data, id)
        return {
            'status': 'success',
            'message': 'Secede log successfully handled',
        }, 200


@UserDto.api.route('/nickname-change')
@UserDto.api.response(500, 'Internal server error')
class UserNicknameChangeAPI(Resource):

    @http_exception_handler
    @login_required
    @UserDto.api.doc(response={
        200: 'User Nickname successfully changed',
        400: 'Data validation failed',
        401: 'Login required',
    })
    def put(self):
        """ User Nickname 변경 """
        # Setter
        data = request.json
        # Action
        UserService.change_nickname(g.user, data)
        return {
            'status': 'success',
            'message': 'User Nickname successfully changed',
        }, 200
