import uuid
from . import db, User
from app.main.util import trim_phone_number
from ..auth.models import UserSecedeLog, AccountStatus, SecedeStatus
from sqlalchemy import or_


class UserService:

    @staticmethod
    def get_all_users(args):
        users = User.query
        if not args.get('q'):
            return users.order_by(User.id.desc()).all()
        # 검색 쿼리 있을 경우
        if args.get('q'):
            q_set = args.get('q').split(',')
            for q in q_set:
                k, v = q.split('=')
                if k == 'general':
                    try:
                        v = int(v)
                    except:
                        users = users.filter(
                            User.nickname.like('%{}%'.format(v)),
                        )
                    else:
                        users = users.filter(
                            or_(
                                User.id == v,
                                User.phone_number.like('%{}%'.format(v)),
                            )
                        )
                else:
                    users = users.filter(getattr(User, k).like('%{}%'.format(v)))
        return users.order_by(User.id.desc()).all()

    @staticmethod
    def create_new_user(data):
        phone_code = data['phone_code']
        phone_number = trim_phone_number(data['phone_number'])
        new_user = User(
            public_id=str(uuid.uuid4()),
            phone_code=phone_code,
            phone_number=phone_number,
            password=data['password'],
            nickname=data['nickname'],
            lang=data['lang'],
            preferred_country=data['preferred_country'],
            agree_terms=data['agree_terms'],
            agree_privacy=data['agree_privacy'],
            agree_sms_notice=data['agree_sms_notice'],
            agree_sms_marketing=data['agree_sms_marketing'],
        )
        UserService.save_changes(new_user)
        return new_user

    @staticmethod
    def get_user_by_public_id(public_id):
        return User.query.filter_by(public_id=public_id).first()

    @staticmethod
    def get_user(phone_code, phone_number):
        return User.query.filter_by(phone_code=phone_code, phone_number=trim_phone_number(phone_number)).first()

    @staticmethod
    def save_changes(data):
        db.session.add(data)
        db.session.commit()

    @staticmethod
    def generate_token(user):
        try:
            # generate the auth token
            return user.encode_auth_token(user.id)
        except Exception as e:
            return None

    @staticmethod
    def change_preferred_country(user, data):
        user.preferred_country = data.get('preferred_country')
        db.session.commit()

    @staticmethod
    def change_nickname(user, data):
        user.nickname = data.get('nickname')
        db.session.commit()

    @staticmethod
    def secede_apply(user, reason):
        user.account_status = AccountStatus.SECEDE_APPLIED.value
        secede_log = UserSecedeLog(
            user=user,
            reason=reason,
        )
        UserService.save_changes(secede_log)

    @staticmethod
    def secede_complete(user, data):
        user.account_status = AccountStatus.SECEDED_COMPLETED.value
        db.session.commit()

    @staticmethod
    def get_secede_logs(args):
        secede_logs = UserSecedeLog.query
        if not args.get('q'):
            return secede_logs.order_by(UserSecedeLog.id.desc()).all()
        if args.get('q'):
            q_set = args.get('q').split(',')
            for q in q_set:
                k, v = q.split('=')
                if k == 'general':
                    try:
                        v = int(v)
                    except:
                        secede_logs = secede_logs.filter(
                            or_(
                                UserSecedeLog.reason.like('%{}%'.format(v)),
                                UserSecedeLog.manager.like('%{}%'.format(v)),
                            )
                        )
                    else:
                        secede_logs = secede_logs.filter(
                            or_(
                                UserSecedeLog.id == v,
                                UserSecedeLog.user_id == v,
                            )
                        )
                else:
                    secede_logs = secede_logs.filter(getattr(UserSecedeLog, k).like('%{}%'.format(v)))
        return secede_logs.order_by(UserSecedeLog.id.desc()).all()

    @staticmethod
    def handle_secede_log(data, id):
        import datetime
        secede_log = UserSecedeLog.query.filter_by(id=id).first()
        if not secede_log: return False
        secede_log.secede_status = data.get('secede_status')
        if secede_log.secede_status == SecedeStatus.APPROVED.value:
            secede_log.user.account_status = AccountStatus.SECEDED_COMPLETED.value
        elif secede_log.secede_status == SecedeStatus.REJECTED.value:
            secede_log.user.account_status = AccountStatus.NORMAL.value
        secede_log.handled_at = datetime.datetime.utcnow()
        db.session.commit()
        return secede_log.secede_status

    @staticmethod
    def handle_user(data, public_id):
        user = User.query.filter_by(public_id=public_id).first()
        if not user: return False
        user.is_superuser = data.get('is_superuser')
        db.session.commit()
        return user.is_superuser
