import re


def trim_phone_number(num_str, del_zero=True):
    # 0-9가 아니면 모두 삭제
    num = re.sub('[^0-9]', '', num_str)
    return num[1:] if del_zero and num_str.startswith('0') else num


def request_to_alarm(alarm_type, data, targets, manager, memo=None):
    import requests
    import simplejson as json
    from manage import app
    headers = {'Content-Type': 'application/json; charset=utf-8'}
    alarm_data = {'alarm_type': alarm_type, 'data': data, u'tos': targets, 'manager': manager,
                  'memo': memo, 'server': 'CROSS-BOARD', 'env': app.config['ENV']}
    return requests.post(url=app.config['ALARM_SERVER_URL'], data=json.dumps(alarm_data),
                         verify=False, timeout=30, headers=headers)
