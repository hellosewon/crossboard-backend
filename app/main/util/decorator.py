from functools import wraps
from flask import request, abort, g
from werkzeug.exceptions import HTTPException
from app.main.auth.services import AuthService


def http_exception_handler(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except HTTPException as e:
            raise e
        except Exception as e:
            print(e)
            abort(500, 'Internal server error')
    return decorated


def login_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not g.user:
            return abort(401, 'Login required')
        return f(*args, **kwargs)
    return decorated


def admin_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not g.user:
            return abort(401, 'Login required')
        if not g.user.is_superuser:
            return abort(401, 'Admin required')
        return f(*args, **kwargs)
    return decorated


def check_mobile_verified(redis_key):
    def actual_decorator(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            data = request.json
            phone_code = data.get('phone_code')
            phone_number = data.get('phone_number')
            verification_code = data.get('verification_code')
            saved_code = AuthService.get_mobile_verification_code(phone_code, phone_number, redis_key)
            if not saved_code:
                abort(408, 'Mobile verification code expired')
            if saved_code != verification_code:
                abort(403, 'Mobile verification code mismatch')
            AuthService.delete_mobile_verification_code(phone_code, phone_number, redis_key)
            return f(*args, **kwargs)
        return decorated
    return actual_decorator


def marshmallow_validate(schema, data_type):
    def actual_decorator(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            data = getattr(request, data_type)
            print(data)
            errors = schema().validate(data)
            if errors:
                print(errors)
                abort(400, errors)
            return f(*args, **kwargs)
        return decorated
    return actual_decorator


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        data, status = AuthService.get_logged_in_user(request)
        token = data.get('data')

        if not token:
            return data, status

        return f(*args, **kwargs)

    return decorated


def admin_token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        data, status = AuthService.get_logged_in_user(request)
        token = data.get('data')

        if not token:
            return data, status

        admin = token.get('admin')
        if not admin:
            response_object = {
                'status': 'fail',
                'message': 'admin token required'
            }
            return response_object, 401

        return f(*args, **kwargs)

    return decorated
