from app.main.board.services import BoardService, PostService, CommentService


boards = [
    {
        'board_group': 'VN',
        'url_namespace': 'vn-life',
        'name': '생활정보',
    },
    {
        'board_group': 'VN',
        'url_namespace': 'vn-region',
        'name': '지역정보',
    },
    {
        'board_group': 'VN',
        'url_namespace': 'vn-free',
        'name': '자유게시판',
    },
    {
        'board_group': 'PH',
        'url_namespace': 'ph-life',
        'name': '생활정보',
    },
    {
        'board_group': 'PH',
        'url_namespace': 'ph-region',
        'name': '지역정보',
    },
    {
        'board_group': 'PH',
        'url_namespace': 'ph-free',
        'name': '자유게시판',
    },
    {
        'board_group': 'TH',
        'url_namespace': 'th-life',
        'name': '생활정보',
    },
    {
        'board_group': 'TH',
        'url_namespace': 'th-region',
        'name': '지역정보',
    },
    {
        'board_group': 'TH',
        'url_namespace': 'th-free',
        'name': '자유게시판',
    },
    {
        'board_group': 'NP',
        'url_namespace': 'np-life',
        'name': '생활정보',
    },
    {
        'board_group': 'NP',
        'url_namespace': 'np-region',
        'name': '지역정보',
    },
    {
        'board_group': 'NP',
        'url_namespace': 'np-free',
        'name': '자유게시판',
    },
]

posts = [
    {
        "title": "하나에 나의 별 가난한 있습니다.",
        "content": "<p>하나에 나의 별 가난한 있습니다. 헤일 부끄러운 비둘기, 계십니다. 별 이 하나에 밤을 사랑과 별을 가난한 그러나 지나가는 까닭입니다. 위에 북간도에 하나 사랑과 하나에 위에 이름자를 있습니다. 자랑처럼 이름과, 토끼, 듯합니다. 까닭이요, 가을 하나에 무엇인지 언덕 위에 피어나듯이 멀듯이, 듯합니다. 다하지 별이 동경과 봅니다. 무덤 하늘에는 무성할 사람들의 어머니, 추억과 별을 이웃 릴케 있습니다. 가을로 하늘에는 이국 별 봅니다. 많은 아무 노새, 오는 나는 자랑처럼 묻힌 이름과, 봅니다. 아이들의 어머니, 밤이 멀리 이런 다 봅니다.</p>",
        "anon_author": "관리자",
        "anon_password": "1234",
        "is_notice": True,
        "is_recommended": False,
    },
    {
        "title": "차 멀리 위에 된 어머니 비둘기, 별 가난한 이국 까닭입니다.",
        "content": "<p>차 멀리 위에 된 어머니 비둘기, 별 가난한 이국 까닭입니다. 다 이름과, 멀리 토끼, 차 내일 별 사람들의 봅니다. 내일 그러나 슬퍼하는 별을 하나에 남은 북간도에 내린 거외다. 어머니, 사람들의 다 써 계십니다. 사람들의 많은 없이 별 남은 별 언덕 가슴속에 가난한 봅니다. 무엇인지 별에도 이국 없이 새워 멀리 하나 봅니다. 많은 별을 아직 별 시인의 아침이 별 때 있습니다. 멀리 가슴속에 추억과 어머님, 너무나 써 아침이 차 봅니다. 부끄러운 북간도에 별 내린 않은 하나에 토끼, 지나가는 있습니다.</p>",
        "anon_author": "관리자",
        "anon_password": "1234",
        "is_notice": True,
        "is_recommended": False,
    },
    {
        "title": "이 헌법시행 당시의 법령",
        "content": "<p>이 헌법시행 당시의 법령과 조약은 이 헌법에 위배되지 아니하는 한 그 효력을 지속한다. 모든 국민은 사생활의 비밀과 자유를 침해받지 아니한다. 정부는 회계연도마다 예산안을 편성하여 회계연도 개시 90일전까지 국회에 제출하고, 국회는 회계연도 개시 30일전까지 이를 의결하여야 한다. 헌법재판소에서 법률의 위헌결정, 탄핵의 결정, 정당해산의 결정 또는 헌법소원에 관한 인용결정을 할 때에는 재판관 6인 이상의 찬성이 있어야 한다.</p>",
        "anon_author": "해리포터",
        "anon_password": "1234",
        "is_notice": False,
        "is_recommended": True,
    },
    {
        "title": "된 하나에 동경과 이름과 어머니, 노새, 나는 별을 봅니다.",
        "content": "<p>된 하나에 동경과 이름과 어머니, 노새, 나는 별을 봅니다. 아름다운 겨울이 어머니 묻힌 둘 까닭입니다. 아직 내 파란 패, 봄이 한 있습니다. 쓸쓸함과 겨울이 어머니, 계십니다. 남은 멀리 흙으로 내 슬퍼하는 하나 듯합니다. 동경과 계집애들의 흙으로 헤는 이름과, 있습니다. 위에 차 옥 써 잠, 이네들은 까닭이요, 밤이 시인의 봅니다. 어머니, 가난한 노새, 하나에 않은 봅니다. 사람들의 언덕 많은 소녀들의 아무 버리었습니다. 이런 못 무성할 별 잔디가 하나에 어머님, 별들을 지나고 버리었습니다. 별들을 강아지, 헤일 지나고 이름과, 벌써 소녀들의 아침이 사랑과 있습니다.</p>",
        "anon_author": "헤르미온느",
        "anon_password": "1234",
        "is_notice": False,
        "is_recommended": True,
    },
    {
        "title": "한 없이 당신은 경, 가을로 이런 내린 불러 때 봅니다.",
        "content": "<p>한 없이 당신은 경, 가을로 이런 내린 불러 때 봅니다. 이름과 마리아 가을로 거외다. 그러나 헤일 무엇인지 까닭입니다. 덮어 애기 계절이 아직 파란 하늘에는 같이 까닭입니다. 차 멀리 이런 속의 내 있습니다. 이제 별 어머님, 위에도 나는 비둘기, 계집애들의 버리었습니다. 이름과, 어머니 하나의 별 불러 언덕 나는 위에도 너무나 버리었습니다. 풀이 우는 노루, 있습니다. 멀듯이, 겨울이 밤이 밤을 듯합니다.</p>",
        "anon_author": "볼드모트",
        "anon_password": "1234",
        "is_notice": False,
        "is_recommended": True,
    },
    {
        "title": "이름자 강아지, 이국 오면 이름을 애기 아이들의 버리었습니다.",
        "content": "<p>이름자 강아지, 이국 오면 이름을 애기 아이들의 버리었습니다. 벌써 겨울이 멀리 어머님, 하나 내일 위에도 둘 버리었습니다. 별빛이 마디씩 하나에 계십니다. 남은 파란 계절이 하나에 옥 나는 멀리 봅니다. 않은 이웃 릴케 이름을 남은 별 하나 내 동경과 까닭입니다. 별을 노루, 새워 별 비둘기, 아침이 가을로 불러 까닭입니다. 이름자를 풀이 걱정도 새워 있습니다. 어머니, 패, 이름과, 버리었습니다. 나는 흙으로 너무나 듯합니다. 하늘에는 애기 그러나 오는 가을로 언덕 계십니다. 이네들은 피어나듯이 이름자 지나가는 어머니 프랑시스 있습니다.</p>",
        "anon_author": "론 위즐리",
        "anon_password": "1234",
        "is_notice": False,
        "is_recommended": True,
    },
]


for board in boards:
    BoardService.create_new_board(board)

for post in posts:
    BoardService.create_new_board_post(7, post)
