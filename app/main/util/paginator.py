import math


class Paginator:
    # TODO: Improve

    def __init__(self, page_size=10):
        self.page_size = page_size

    def gen_response(self, data, current_page=1, key='data'):
        start = (current_page - 1) * self.page_size
        end = current_page * self.page_size
        return {
            key: data[start:end],
            'page_size': self.page_size,
            'total_pages': math.ceil(len(data) / self.page_size),
            'current_page': current_page,
        }
