from marshmallow import fields
from marshmallow.validate import Length, OneOf, Range, Regexp

from app.main.board.models import BoardStatus


class AuthValidator:
    phone_code = fields.Str(required=True, validate=[Length(min=1, max=5), OneOf(['82'])])
    phone_number = fields.Str(required=True, validate=[Length(min=9, max=13), Regexp('^[0-9- ]+$')])
    password = fields.Str(required=True, validate=[Length(min=6, max=30), Regexp('^\w+$')])

    mobile_verification_code = fields.Str(required=True, validate=[Length(min=1, max=6), Regexp('^\d+$')])
    mobile_verification_purpose = fields.Str(required=True, validate=[OneOf(['REGISTRATION', 'PASSWORD_RESET', 'PHONE_CHANGE'])])


class UserValidator:
    nickname = fields.Str(required=True, validate=[Length(min=2, max=16), Regexp('^\w+$')])
    lang = fields.Str(required=True, validate=[OneOf(['en', 'ko', 'th', 'vi'])])  # TODO: OneOf DB?
    preferred_country = fields.Str(required=True, validate=[OneOf(['TH', 'VN', 'NP', 'PH'])])  # TODO: OneOf DB?
    agree_terms = fields.Bool(required=True, validate=[OneOf(set(fields.Bool.truthy))])
    agree_privacy = fields.Bool(required=True, validate=[OneOf(set(fields.Bool.truthy))])
    agree_sms_notice = fields.Bool(required=True)
    agree_sms_marketing = fields.Bool(required=True)


class BoardValidator:
    board_group = fields.Str(required=True, validate=[Length(min=1, max=100)])
    name = fields.Str(required=True, validate=[Length(min=1, max=100)])
    url_namespace = fields.Str(required=True, validate=[Length(min=1, max=100)])
    board_status = fields.Int(required=False, validate=[Range(BoardStatus.unactivated.value, BoardStatus.activated.value)])  # TODO: OneOf constant values?


class PostValidator:
    is_notice = fields.Bool(required=False)
    is_recommended = fields.Bool(required=False)
    is_popular = fields.Bool(required=False)
    limit = fields.Int(required=False, validate=[Range(0, 100)])
    page = fields.Int(required=False, validate=[Range(1)])
    label = fields.Str(required=False, validate=[Length(min=1, max=20)])  # TODO: OneOf DB?
    anon_author = fields.Str(required=False, validate=[Length(min=2, max=16), Regexp('^\w+$')])
    anon_password = fields.Str(required=False, validate=[Length(min=6, max=16), Regexp('^\w+$')])
    post_label = fields.Str(required=True, validate=[Length(min=1, max=20)])  # TODO: OneOf DB?
    title = fields.Str(required=True, validate=[Length(min=1, max=255)])
    content = fields.Str(required=True, validate=[Length(min=8, max=20000)])
    q = fields.Str(required=False, validate=[Length(min=2, max=120)])


class CommentValidator:
    anon_author = fields.Str(required=False, validate=[Length(min=2, max=16), Regexp('^\w+$')])
    anon_password = fields.Str(required=False, validate=[Length(min=6, max=16), Regexp('^\w+$')])
    content = fields.Str(required=True, validate=[Length(min=2, max=2000)])
