# -*- coding: utf-8 -*
from fabric.api import *

env.roledefs = {
    'dev': ['ubuntu@15.165.19.70'],
    'real': ['ubuntu@52.79.48.40', 'ubuntu@52.78.19.179']
}
env.key_filename = '/var/product/cross-aws-keypair.pem'
env.user = 'ubuntu'


@hosts(['ubuntu@15.165.19.70'])
def dev():
    with cd("/home/ubuntu/crossboard-backend"):
        run("sudo service cross stop")
        run("git pull")
        run("venv/bin/pip3.6 install -r requirements.txt")
        run("sudo service cross start")


@hosts(['ubuntu@52.79.48.40', 'ubuntu@52.78.19.179'])
def real():
    with cd("/home/ubuntu/crossboard-backend"):
        run("sudo service cross stop")
        run("git pull")
        run("venv/bin/pip3.6 install -r requirements.txt")
        run("sudo service cross start")
